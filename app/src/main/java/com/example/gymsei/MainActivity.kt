package com.example.gymsei

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.sp
import androidx.core.text.isDigitsOnly
import com.example.gymsei.ui.theme.GymSEITheme
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.delay


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GymSEITheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Greeting()
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Greeting() {



    var userTable: MutableList<User> by remember { mutableStateOf(mutableListOf()) }
    var sortedUserTable: MutableList<User> by remember { mutableStateOf(mutableListOf()) }
    //Dont know how or why i can create a function within a function, but it works for me
        suspend fun userTableUpdate() {

            val request = Requests()

            val listType = object : TypeToken<List<User>>() {}.type
            val gson = Gson()

            val response = request.apiGet()
            userTable = gson.fromJson(response, listType)
            sortedUserTable = userTable.sortedWith(compareBy{it.lastName}).toMutableList()

        }
    LaunchedEffect(Unit){
        while (true){
            userTableUpdate()
            delay(500)
        }
    }


    Column(modifier = Modifier
        .verticalScroll(rememberScrollState())
    ){

        sortedUserTable.forEach {
            var expanded by remember { mutableStateOf(false) }
            var cardColor by remember { mutableStateOf(Color.Green) }
            if (it.inGym == 0){
            cardColor = Color.Red
            }
            else if (it.inGym == 1){
            cardColor = Color.Green}
            Card (modifier = Modifier
                .fillMaxWidth(),
                onClick = {expanded = !expanded},
                colors = CardDefaults.cardColors(
                    containerColor = cardColor
                )
            ){

                Column {
                    Text(text = "${it.lastName}, ${it.firstName}",
                    fontSize = 50.sp, fontFamily = FontFamily.Serif
                    )
                    if (expanded) {
                    if (it.inGym == 0){
                        Text(text = "NOT IN GYM")}
                    else {
                        Text(text = "CURRENTLY IN GYM")
                    }
                        var pinInsert by remember { mutableStateOf("") }
                        var textLabel by remember { mutableStateOf("Enter Pin Here") }
                        Row {
                        TextField(
                            value = pinInsert,
                            onValueChange = { it -> pinInsert = it },
                            label = { Text(text = textLabel) },
                            visualTransformation = PasswordVisualTransformation(),
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword)
                        )

                            Button(onClick = {

                                try {
                                    if (it.pin == pinInsert.toInt()) {
                                        Requests().apiUpdate(
                                            UpdateUser(
                                                it.userID,
                                                it.firstName,
                                                it.inGym,
                                                it.pin
                                            )
                                        );
                                        expanded = !expanded
                                        textLabel = "Enter Pin Here"
                                    }
                                    else{
                                        textLabel = "Wrong Pin Entered"
                                        pinInsert = ""
                                    }
                                }
                                catch (e: Exception){
                                    println("Error: ${e.message}")
                                    Log.e("LOG", e.message.toString())
                                }


                            }) {
                                Text(text = "LOGIN!")
                            }//End of LOGIN BUTTON

                    }
                    }//end of expanded
                }
                    if (expanded){
                    Button(onClick = { Requests().apiRemove(User(it.userID,it.firstName,it.lastName,it.pin,it.inGym)); Log.i("HELP", "${it.userID} ${it.firstName} ${it.lastName} ${it.pin} ${it.inGym}")}) {
                        Text(text = "Remove USER")
                    }
                    }// end of Expanded
                }
        }//End of Card Creation "for.each" user in database
        Row(){

            var newCard by remember { mutableStateOf(false) }
            var newUserFirst by remember { mutableStateOf("") }
            var newUserLast by remember { mutableStateOf("") }
            var newUserPin by remember { mutableStateOf("") }
            val pattern = remember { Regex("[a-zA-Z//s]*") }
            Button(onClick = {
                if (!newCard)
                {newCard = !newCard}

                else {
                    newUserFirst = ""
                    newUserLast = ""
                    newUserPin = ""
                    newCard = !newCard}
                             }) {

                Text(text = "ADD NEW GYM MEMBER")
            }

            if (newCard){
                Card {

                    TextField(
                    value = newUserFirst,
                    onValueChange = { if (it.matches(pattern)) { newUserFirst = it}  },
                    label = { Text(text = "First Name (Letters Only)") },
                )

                    TextField(
                        value = newUserLast,
                        onValueChange = {
                            if (it.matches(pattern)) { newUserLast = it} },
                        //keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Ascii),
                        label = { Text(text = "Last Name (Letters Only)") },
                    )

                    TextField(
                        value = newUserPin,
                        onValueChange = { newUserPin = it },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.NumberPassword),
                        label = { Text(text = "Create Pin (7 Digits)") },
                    )
                    Button(onClick = {try {

                    if (newUserPin.length == 7 && newUserPin.isDigitsOnly()){
                        Requests().apiNewUser(NewGymUser((4..1000000000).random(), newUserFirst,newUserLast,newUserPin.toInt(),1))
                    }}
                    catch (e: Error){Log.i("NEW PIN ERROR",e.message.toString())}
                    })
                    {Text(text = "SUBMIT")
                    }
                }}


        }
    }
}

