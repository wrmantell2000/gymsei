package com.example.gymsei

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class User(
    @SerialName("userID") val userID: Int ,
    @SerialName("firstName") val firstName: String,
    @SerialName("lastName") val lastName: String,
    @SerialName("pin") val pin: Int,
    @SerialName("inGym") val inGym: Int
){
}
