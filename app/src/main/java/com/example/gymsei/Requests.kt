package com.example.gymsei

import android.util.Log
import io.ktor.client.HttpClient
import io.ktor.client.features.ClientRequestException
import io.ktor.client.request.delete
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readText
import io.ktor.http.ContentType
import io.ktor.http.contentType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class Requests {
    suspend fun apiGet(): String {
        val client = HttpClient()
        var info: String = ""
        try {
            coroutineScope {
                val response: HttpResponse = client.get("http://10.0.2.2:8080/gym/users")
                info = response.readText()
            }
            //print("response: $responseData")
        } catch (e: ClientRequestException) {
            println("Failed to make request: ${e.response.readText()}")
        } finally {
            client.close()
        }
        return info
    }

    fun apiNewUser(newUser: NewGymUser) {
        val client = HttpClient()
        val json = Json
        val login = json.encodeToString(newUser)

        runBlocking(Dispatchers.IO) {
            try {
                val response = client.post<String>("http://10.0.2.2:8080/gym/newuser") {
                    body = (login)
                    contentType(ContentType.Application.Json)
                }
                println(response)
            } catch (e: Exception) {
                println("Error: ${e.message}")
            }//end of catch
            finally {
                client.close()
            }
        }//run Blocking
    }

    fun apiUpdate(user: UpdateUser) {
        val client = HttpClient()
        val json = Json
        val checkin = json.encodeToString(user)

        runBlocking(Dispatchers.IO) {
            try {
                val response = client.post<String>("http://10.0.2.2:8080/gym/updateuser") {
                    body = (checkin)
                    contentType(ContentType.Application.Json)
                }
                println(response)
            } catch (e: Exception) {
                println("Error: ${e.message}")
            }//end of catch
            finally {
                client.close()
            }
        }//run Blocking
    }


    fun apiRemove(user: User) {
        val client = HttpClient()
        val json = Json
        val remove = json.encodeToString(user)

        runBlocking(Dispatchers.IO) {
            try {
                val response = client.delete<String>("http://10.0.2.2:8080/gym/removeuser") {
                    body = (remove)
                    contentType(ContentType.Application.Json)
                    Log.i("HELP", remove)
                }
                println(response)
            } catch (e: Exception) {
                println("Error: ${e.message}")
            }//end of catch
            finally {

                client.close()
            }
        }//run Blocking
    }
}

